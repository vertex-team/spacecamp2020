import React from "react";
import "./styles/home.css";
import arrowicon from "../assets/images/arrow-right-circle.svg";
import foodicon from "../assets/images/icons8-wheat-100.png";
import watericon from "../assets/images/icons8-water-100.png";
import handicon from "../assets/images/icons8-hand-100.png";
import { Link } from "react-router-dom";

export default function Home() {
  return (
    <div className="home">
      <div className="home-content">
        <h1 className="title">UFWEMS</h1>
        <p>
          Earth is dying, we are short in food, water and energy.
          <br /> Do your part by signing up for the system.
        </p>
        <ol>
          <li>
            Sign Up for the UFWEMS as it is part of government efforts to
            mitigate food and water shortages around the globe.{" "}
          </li>
          <br />
          <li>
            Effective by February 1st 2050, supermarkets will no longer supply
            food and water, instead citizens will have to collect daily supplies
            from the nearest Food and Water Station.
          </li>
          <br />
          <li>
            After Signing Up, you will have to log in into the system. From
            there, you can receive:
            <br /> Daily weather reports <br />
            Food Storage
            <br /> Information Water Storage <br />
            Information Real time water quality <br />
            Information Forecast of any upcoming natural disaster Notifications
            of any dangerous natural disaster situations nearby <br />
            General advice on Mitigating food and water shortage
          </li>
        </ol>
        <div className="home-info-sec">
          <div className="home-info-container">
            <div className="home-info-content">
              <div className="home-info-icon">
                <img src={foodicon} alt={"Food Program"} draggable={false} />
              </div>
              <div className="home-info-sec1">
                <h1 className="home-info-title">Food Program</h1>
                <h4>
                  Nearest Distribution Station: Chung Ling High School Penang
                </h4>
                <h4>Food Availability: Yes</h4>
              </div>
              <div className="home-info-sec2">
                <Link to="/food_program">
                  <img
                    src={arrowicon}
                    alt="See More Button"
                    draggable={false}
                  />
                </Link>
              </div>
            </div>
          </div>
        </div>
        <div className="home-info-sec">
          <div className="home-info-container">
            <div className="home-info-content">
              <div className="home-info-icon">
                <img src={watericon} alt={"Water Program"} draggable={false} />
              </div>
              <div className="home-info-sec1">
                <h1 className="home-info-title">Water Program</h1>
                <h4>
                  Nearest Distribution Station: Chung Ling High School Penang
                </h4>
                <h4>Water Availability: Yes</h4>
              </div>
              <div className="home-info-sec2">
                <Link to="/water_program">
                  <img
                    src={arrowicon}
                    alt="See More Button"
                    draggable={false}
                  />
                </Link>
              </div>
            </div>
          </div>
        </div>
        <div className="home-info-sec">
          <div className="home-info-container">
            <div className="home-info-content">
              <div className="home-info-icon">
                <img src={handicon} alt={"Pollutions"} draggable={false} />
              </div>
              <div className="home-info-sec1">
                <h4>Nearest Major Water Source: Hoover Dam, Colorado River</h4>
                <h4>Water Quality Index: 52</h4>
              </div>
              <div className="home-info-sec2">
                <Link to="/water_pollution">
                  <img
                    src={arrowicon}
                    alt="See More Button"
                    draggable={false}
                  />
                </Link>
              </div>
            </div>
          </div>
        </div>
        <div className="home-blank">&nbsp;</div>
      </div>
    </div>
  );
}
