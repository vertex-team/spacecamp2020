import React, { useState } from "react";
import "./styles/waterprogram.css";
import map from "../assets/images/map-3.jpg";
import Pin from "./Pin";

export default function WaterProgram() {
  const [location, setLocation] = useState(
    "Chung Ling High School Penang, Malaysia"
  );
  const [value, setValue] = useState(12);
  function handleClick(country) {
    switch (country) {
      case "indonesia":
        setLocation(
          "pasar sambeani, Sambeani, Abuki, Konawe Regency, South East Sulawesi 93452, Indonesia"
        );
        setValue(10);
        break;
      case "india":
        setLocation(
          "Madras High Court, High Ct Rd, Parry's Corner, George Town, Chennai, Tamil Nadu 600108, India"
        );
        setValue(13);
        break;
      case "thailand":
        setLocation("Bang Kapi District, 10240 Bangkok ,Thailand ");
        setValue(9);
        break;
      case "malaysia":
        setLocation("Chung Ling High School Penang, Malaysia");
        setValue(12);
        break;
      case "philippines":
        setLocation(
          "Ang Panublion Museum,Legazpi Street, Roxas City, Capiz, Philippines"
        );
        setValue(8);
        break;
      case "singapore":
        setLocation(
          "Yishun Town Secondary School, 6 Yishun Street 21, Singapore "
        );
        setValue(14);
        break;
      default:
        setLocation(null);
        setValue(null);
        break;
    }
  }

  return (
    <div className="water-program">
      <h1 className="water-program-title">Water Program</h1>
      <p className="nearest">Nearest Station: {location}</p>
      <div className="map-container">
        <Pin color="yellow" country="indonesia" onClick={handleClick} />
        <Pin color="green" country="india" onClick={handleClick} />
        <Pin color="yellow" country="thailand" onClick={handleClick} />
        <Pin color="green" country="malaysia" onClick={handleClick} />
        <Pin color="yellow" country="philippines" onClick={handleClick} />
        <Pin color="green" country="singapore" onClick={handleClick} />
        <img src={map} alt="map" className="map" draggable={false} />
      </div>
      <div className="others">
        <div className="details">
          <p>Volume of water left: {value} tonnes</p>
          <p>
            Number of portions that can be served:{" "}
            {(_ => {
              if (value >= 12) return 100;
              else if (value < 12 && value >= 8) return 80;
              else if (value < 8 && value >= 4) return 40;
            })()}{" "}
            million
          </p>
        </div>
        <div className="instructions">
          <p>Instructions:</p>
          <ol type="1">
            <li>
              Water collection starts at 8am sharp, please bring your NRIC or
              Mobile phone with the UFWEMS App for scanning. Only NRIC and QR
              Codes are accepted.
            </li>
            <li>Line up orderly at every counter.</li>
            <li>Each citizen is only entitled for 2L of water a day</li>
            <li>
              For household usage, 500L is the limit for each house a day.
            </li>
            <li>
              For industrial usage, please arrange with the local government
              through this URL <br />
              industrih2o.gov.my
            </li>
          </ol>
        </div>
      </div>
    </div>
  );
}
