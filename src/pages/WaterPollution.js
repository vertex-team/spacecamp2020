import React, { useState } from "react";
import "./styles/waterpollution.css";
import map from "../assets/images/map-3.jpg";
import phGraph from "../assets/images/PH.png";
import o2Graph from "../assets/images/O2.png";
import tempGraph from "../assets/images/TEMP.png";
import turbidGraph from "../assets/images/TURBIDITY.png";
import Pin from "./Pin";

export default function WaterPollution() {
  const [location, setLocation] = useState("Hoover Dam, Colorado River");
  const [value, setValue] = useState(52);
  function handleClick(country) {
    switch (country) {
      case "indonesia":
        setLocation(
          "pasar sambeani, Sambeani, Abuki, Konawe Regency, South East Sulawesi 93452, Indonesia"
        );
        setValue(93);
        break;
      case "india":
        setLocation("Yeleru Reservoir, Andhra Pradesh");
        setValue(80);
        break;
      case "thailand":
        setLocation("Chao Phraya River ,Thailand ");
        setValue(67);
        break;
      case "malaysia":
        setLocation("Hoover Dam, Colorado River");
        setValue(52);
        break;
      case "philippines":
        setLocation("AJalaur River, Philippines");
        setValue(45);
        break;
      case "singapore":
        setLocation("Singapore River, Singapore ");
        setValue(89);
        break;
      default:
        setLocation(null);
        setValue(null);
        break;
    }
  }
  return (
    <div className="water-pollution">
      <h1 className="water-pollution-title">Water Pollution Monitor</h1>
      <p className="nearest">Water source: {location}</p>
      <div className="map-container">
        <Pin color="green" country="indonesia" onClick={handleClick} />
        <Pin color="yellow" country="india" onClick={handleClick} />
        <Pin color="yellow" country="thailand" onClick={handleClick} />
        <Pin color="red" country="malaysia" onClick={handleClick} />
        <Pin color="red" country="philippines" onClick={handleClick} />
        <Pin color="green" country="singapore" onClick={handleClick} />
        <img src={map} alt="map" className="map" draggable={false} />
      </div>
      <div className="table">
        <p className="table-title">Water Quality Index: {value}</p>
        <p className="table-sub">Average, contaminants present</p>
        <div className="table-box">
          <p>
            PH:
            <br /> <img src={phGraph} draggable={false} alt="graph" />
          </p>
          <p>
            Temperature:
            <br /> <img src={tempGraph} draggable={false} alt="graph" />
          </p>
          <p>
            Dissolved Oxygen:
            <br /> <img src={o2Graph} draggable={false} alt="graph" />
          </p>
          <p>
            Turbidity:
            <br /> <img src={turbidGraph} draggable={false} alt="graph" />
          </p>
        </div>
      </div>
    </div>
  );
}
