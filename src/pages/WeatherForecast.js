import React from "react";
import "./styles/weatherforecast.css";
import ReactWeather from 'react-open-weather';
import 'react-open-weather/lib/css/ReactWeather.css';

export default function WeatherForecast() {
    return (
        <div className="weather-forecast">
            <h1 className="weather-forecast-title">Weather Forecast</h1>
            <div className={"weather-widget-sec"}>
                <div className={"weather-widget-box"}>
                    <ReactWeather
                        forecast="5days"
                        apikey="b4c2e86dc3464f0a2b14d28189e972a6"
                        type="city"
                        city="Bayan Lepas"/>
                </div>
            </div>
        </div>
    );
}