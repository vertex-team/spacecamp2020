import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import * as serviceWorker from "./serviceWorker";
import { HashRouter as Router, Route } from "react-router-dom";
import Header from "./pages/Header";
import Home from "./pages/Home";
import FoodProgram from "./pages/FoodProgram";
import WaterProgram from "./pages/WaterProgram";
import WeatherForecast from "./pages/WeatherForecast";
import WaterPollution from "./pages/WaterPollution";
import Notifications from "./pages/Notifications";

ReactDOM.render(
  <Router>
    <div className="content">
      <Header />
      <Route exact path={"/"}>
        <Home />
      </Route>
      <Route path={"/food_program"}>
        <FoodProgram />
      </Route>
      <Route path={"/water_program"}>
        <WaterProgram />
      </Route>
      <Route path={"/weather_forecast"}>
        <WeatherForecast />
      </Route>
      <Route path={"/water_pollution"}>
        <WaterPollution />
      </Route>
      <Route path={"/notifications"}>
        <Notifications />
      </Route>
    </div>
  </Router>,
  document.getElementById("root")
);

serviceWorker.unregister();
